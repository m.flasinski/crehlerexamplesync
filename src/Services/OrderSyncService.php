<?php


namespace Crehler\ExampleSync\Services;


use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;

class OrderSyncService
{
    private EntityRepositoryInterface $orderRepository;

    public function __construct(EntityRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function sync(string $orderId, Context $context)
    {
        // Fetch order
        $order = $this->orderRepository->search(new Criteria([$orderId]), $context);

        // Send data to GetResponse
        // https://apireference.getresponse.com/#tag/Orders

        // Save getResponseID
        $this->orderRepository->upsert([[
            'id' => $orderId,
            'customFields' => [
                'getResponse' => [
                    'id' => 'ID_FROM_GET_RESPONSE'
                ]
            ]
        ]], $context);

    }
}
