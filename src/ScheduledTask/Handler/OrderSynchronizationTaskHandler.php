<?php


namespace Crehler\ExampleSync\ScheduledTask\Handler;


use Crehler\ExampleSync\ScheduledTask\OrderSynchronizationTask;
use Crehler\ExampleSync\Services\OrderSyncService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;
use Shopware\Core\System\SystemConfig\SystemConfigService;

class OrderSynchronizationTaskHandler extends ScheduledTaskHandler
{
    private OrderSyncService $orderSyncService;
    private EntityRepositoryInterface $orderRepository;
    private SystemConfigService $systemConfigService;

    public function __construct(EntityRepositoryInterface $scheduledTaskRepository, OrderSyncService $orderSyncService, EntityRepositoryInterface $orderRepository, SystemConfigService $systemConfigService)
    {
        parent::__construct($scheduledTaskRepository);
    }

    public static function getHandledMessages(): iterable
    {
        return [ OrderSynchronizationTask::class ];
    }

    public function run(): void
    {
        $context = Context::createDefaultContext();
        $lastSyncDate = $this->systemConfigService->get('CrehlerExampleSync.config.orderLastSync');
        $criteria = new Criteria();
        if (!empty($lastSyncDate)) {
            $criteria->addFilter(
                new MultiFilter(
                    MultiFilter::CONNECTION_OR,
                    [
                        new RangeFilter('createdAt', [
                            RangeFilter::GTE => $lastSyncDate,
                        ]),
                        new RangeFilter('updatedAt', [
                            RangeFilter::GTE => $lastSyncDate,
                        ]),
                    ]
                )
            );
        }
        $ordersIdsSearchResult = $this->orderRepository->searchIds($criteria, $context);
        foreach ($ordersIdsSearchResult->getIds() as $id) {
            $this->orderSyncService->sync($id, $context);
        }

        $this->systemConfigService->set('CrehlerExampleSync.config.orderLastSync', (new \DateTime())->format('Y-m-d h:i:s'));
    }
}
