<?php


namespace Crehler\ExampleSync\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;


class OrderSynchronizationTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'getresponse.order_synchronization';
    }

    public static function getDefaultInterval(): int
    {
        return 86400; // 1 day
    }
}

