<?php


namespace Crehler\ExampleSync\Subscriber;


use Crehler\ExampleSync\MessageQueue\OrderSynchronizationMessage;
use Shopware\Core\Checkout\Order\OrderEvents;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderWrittenSubscriber implements EventSubscriberInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderEvents::ORDER_WRITTEN_EVENT => 'onWrittenOrder'
        ];
    }

    public function onWrittenOrder(EntityWrittenEvent $event)
    {
        foreach ($event->getWriteResults() as $entityWriteResult) {
            $this->messageBus->dispatch(new OrderSynchronizationMessage($entityWriteResult->getPrimaryKey(), $event->getContext()));
        }
    }


}
