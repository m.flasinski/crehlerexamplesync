<?php


namespace Crehler\ExampleSync\MessageQueue;


use Shopware\Core\Framework\Context;

class OrderSynchronizationMessage
{
    private string $id;

    private string $contextData;

    public function __construct(string $id, Context $context)
    {
        $this->id = $id;
        $this->withContext($context);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getContextData(): string
    {
        return $this->contextData;
    }

    public function withContext(Context $context): OrderSynchronizationMessage
    {
        $this->contextData = serialize($context);

        return $this;
    }

    public function readContext(): Context
    {
        return unserialize($this->contextData);
    }
}
