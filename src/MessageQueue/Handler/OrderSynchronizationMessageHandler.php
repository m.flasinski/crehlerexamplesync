<?php


namespace Crehler\ExampleSync\MessageQueue\Handler;

use Crehler\ExampleSync\MessageQueue\OrderSynchronizationMessage;
use Crehler\ExampleSync\Services\OrderSyncService;
use Shopware\Core\Framework\MessageQueue\Handler\AbstractMessageHandler;

class OrderSynchronizationMessageHandler extends AbstractMessageHandler
{
    private OrderSyncService $orderSyncService;

    public function __construct(OrderSyncService $orderSyncService)
    {
        $this->orderSyncService = $orderSyncService;
    }

    /**
     * @param OrderSynchronizationMessage $message
     */
    public function handle($message): void
    {
        $this->orderSyncService->sync($message->getId(), $message->readContext());
    }

    public static function getHandledMessages(): iterable
    {
        yield OrderSynchronizationMessage::class;
    }
}
